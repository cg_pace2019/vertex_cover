# CMake build setup for vertex_cover.

cmake_minimum_required (VERSION 3.0 FATAL_ERROR)

project(vertex_cover CXX)

set(CMAKE_CXX_STANDARD 11)

set (vertex_cover_VERSION_MAJOR 0)
set (vertex_cover_VERSION_MINOR 0)

# Set extra warning options for g++, enforce C++11 for O(1) std::list::size().
if (CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_GNUCXX)
	ADD_DEFINITIONS("-Wall -W -Wunused -Wpointer-arith -Wcast-qual -Wwrite-strings -Wnon-virtual-dtor -Woverloaded-virtual -Wsign-promo -O3 -g")
endif (CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_GNUCXX)

# Make static executable.
#set (CMAKE_EXE_LINKER_FLAGS "-static")

# Our sources.
add_executable(vertex_cover
    main.cc instance.hh instance.cc vertex.hh vertex.cc
    util.hh util.cc vertexcover.hh vertexcover.cc branching.hh branching.cc
    folding.hh folding.cc
    )


# Enable distribution.
include (InstallRequiredSystemLibraries)
SET(CPACK_PACKAGE_DESCRIPTION_FILE "${CMAKE_CURRENT_SOURCE_DIR}/README.txt")
SET(CPACK_PACKAGE_DESCRIPTION_SUMMARY "A solver for the Minimum Vertex Cover optimization problem")
set (CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/LICENSE.txt")
set (CPACK_PACKAGE_VERSION_MAJOR "${vertex_cover_VERSION_MAJOR}")
set (CPACK_PACKAGE_VERSION_MINOR "${vertex_cover_VERSION_MINOR}")
include (CPack)
