#ifndef __INSTANCE_H
#define __INSTANCE_H

#include <vector>
#include <boost/shared_ptr.hpp>

// Forward references.
class Vertex;
class VertexCover;
class Foldings;

// A single instance of the vertex cover problem.
class Instance {
private:
  unsigned m_nVertices;

  // The vertices of this graph, including their neighbourhood. This is
  // symmetric, so if u is in the neighbourhood of vertex v, then v is in the
  // neighbourhood of vertex u. Note that vertex numbering starts from
  // 1. Loops are possible, i.e. u can be in its own neigbourhood. Duplicate
  // edges are NOT possible.
  Vertex* m_vertices;

  // Find a maximal matching and return the vertices that are NOT in the
  // matching. This is a helper for the crown reduction.
  //
  // mc is an array of size m_nVertices + 1. mc[v] will be set to !=0 IFF
  // vertex v is NOT matched, 0 otherwise.
  void maximalMatchingUnmatchedVertices(unsigned* const mc) const;

  // Find a maximum matching in a bipartite graph (U \cup V, adj), with U, V
  // \subsetq [1 .. N]. For u \in U, adj[u] contains the neighbours of u \in
  // V. For u \in U, MU[u] will be set to the matching node of u in V. For v
  // \in V, MV[v] will be set to the matching node of v in U.
  void findMaximumMatching(const list<unsigned>& U, const list<unsigned>& V,
                           const list<unsigned>* adj,
                           unsigned* MU, unsigned* MV) const;

#ifndef NDEBUG
  // Statistics for reductions and foldings.
  static unsigned long loopReductions;
  static unsigned long deg1Reductions;
  static unsigned long deg2ConnReductions;
  static unsigned long deg3Reductions;
  static unsigned long bussReductions;
  static unsigned long twinReductions;
  static unsigned long unconfinedReductions;
  static unsigned long crownReductions;
  static unsigned long crownReductionSol;
  static unsigned long crownReductionRem;
  static unsigned long cliqueReductions;
  static unsigned long cliqueReductionSol;
  static unsigned long lpReductions;
  static unsigned long lpReductionSol;
  static unsigned long lpReductionRem;
  
  static unsigned long deg2Folds;
  static unsigned long funnelFolds;
  static unsigned long funnelFoldIntersects;
  static unsigned long deskFolds;
public:
  static void initStats();
  static void printStats(FILE* file);
private:
#endif // NDEBUG

public:
  Instance(unsigned nVertices);
  Instance(const Instance& o);
  ~Instance();

  unsigned getNVertices() const { return m_nVertices; }

  // Insert an edge {u, v}. If the edge already exists, nothing is done.
  void insertEdge(unsigned u, unsigned v);

  // Remove an edge {u, v}. The edge must exist.
  void removeEdge(unsigned u, unsigned v);

  // Check whether an edge {u, v} exists in the graph. This is an expensive
  // operation with O(n*n) worst case complexity, so use sparingly (O(n)
  // average).
  bool hasEdge(unsigned u, unsigned v) const;

  // Returns a reference to a vertex.
  const Vertex& getVertex(unsigned v) const;
  
  // Isolate a vertex, i.e. remove all adjacent edges from the graph. This is
  // tantamount to deleting it, as it will be left behind during the next
  // shrink.
  void isolateVertex(unsigned v);
  // As above, but adds the list of vertexes affected by this, i.e. v's
  // neighbourhood, to changes.
  void isolateVertex(unsigned v, list<unsigned>& changes);

  // Isolate all vertices in vs.
  void isolateVertices(const list<unsigned>& vs);
  // As above, but adds the list of vertexes affected by this, i.e. vs'
  // neighbourhoods, to changes.
  void isolateVertices(const list<unsigned>& vs, list<unsigned>& changes);
  
  // Set 'loops' to the list of vertices with loops. This is expensive (O(n*n)).
  void getLoops(list<unsigned>& loops) const;

  // Sets iso to the list of isolated vertices. This is expensive (O(n)).
  void getIsolatedVertices(list<unsigned>& iso) const;

  // Get the vertex with the highest degree.
  unsigned maxDegreeVertex() const;
  
  // Highest vertex degree in the graph.
  unsigned maxDegree() const;

  // Return the neighbour of v with the highest vertex degree. If v has no
  // neighbors, the behavious is undefined.
  unsigned maxDegreeNeighbour(unsigned v) const;

  // Get the vertex with the lowest degree.
  unsigned minDegreeVertex() const;
    
  // Lowest vertex degree in the graph.
  unsigned minDegree() const;

  // Get the vertices with the lowest and highest degree.
  void getMinMaxDegreeVertices(unsigned& minV, unsigned& maxV) const;
  
  // Get the non-isolated vertices with the lowest and highest degree. If
  // there are no non-isolated vertices, then set minV and maxV to 0.
  void getNonIsoMinMaxDegreeVertices(unsigned& minV, unsigned& maxV) const;
  
  // Returns true if the graph is regular, i.e. all vertices have the same
  // degree.
  bool regular() const;

  // Return a vertex  with the degree 2 if such a vertex exists, or 0
  // otherwise.
  unsigned findDegree2Vertex() const;
  
  // Get total number of edges. This is a complex operation, because we have
  // to look for loops.
  unsigned getNEdges() const;

  // On return, e will contain a list of pairs (u, v), each representing the
  // edge {u, v}. There will be no duplicates.
  void getEdges(list<pair<unsigned, unsigned> >& e) const;

  // Set N to N(vs), the open neighbourhood of vs defined as follows:
  // N(vs) := \cup_{v \in vs} N(v) \ vs. N must be empty.
  void getOpenNeighbourhood(const list<unsigned>& vs, list<unsigned>& N) const;

  // Set N to N[vs], the closed neighbourhood of vs defined as follows:
  // N[vs] := N(vs) \cup vs. N must be empty.
  void getClosedNeighbourhood(const list<unsigned>& vs,
                              list<unsigned>& N) const;
  
  // Loop preprocessing. This only has to be called ONCE (right after reading
  // the input graph). Finds all vertices with loops, isolates them, and adds
  // them to vc.
  void removeLoops(VertexCover& vc);

  // Apply the "degree 1" reduction rule to vertex v. If it is applicable,
  // then add its neighbour to vc and also append a list of nodes that have
  // changed in the process to chs. This does NOT make sure that there are no
  // duplicates in chs. It also does not guarentee that there are no nodes
  // with degree 1 in the graph, because some of the changed nodes may have
  // been reduced to degree 1.
  void reduceDegree1(unsigned v, VertexCover& vc, list<unsigned>& chs);

  // Apply the 'degree 2 with connected neighbours' rule to vertex u. If u has
  // degree two, and its neigbours v and w are adjacent, then add v and w to
  // the solution vc, and isolate u. Add a list of affected nodes to chs.
  // Apply the 'degree 2 with disjoint neighbours' rule to vertex u. If u has
  // degree 2, and its neighbours v amd w are not adjacent, then replace u, v,
  // and w with a new node u' that is connected to the union of all neighbours
  // (sans u) of v and w, and has the same vertex number as u. Add a
  // Deg2Folding(u, v, w) to folds. Add a list of affected nodes
  // (i.e. neighbours of v and w) to chs. The caller can use folds to unfold
  // the solution for the reduced graph as follows: if u' is in the vertex
  // cover, then replace it with v and w. If u' is not in the vertex cover,
  // then add u to the vertex cover.
  void reduceDegree2(unsigned u, VertexCover& vc,
                     Foldings& folds, list<unsigned>& chs);


  // Apply the 'degree 3 rule' to vertex u: If u is degree 3, and has one
  // other neighbour w with degree 3, and another neighbour v such that u, v,
  // w form a triangle, and a third neighbour x that is connected to the third
  // neighbour y of w, then return v. Otherwise, return 0.
  unsigned findDegree3Reduction(unsigned u) const;

  // If findDegree3Reduction(u) returns v != 0, then add v to vc, N(v) to chs,
  // and isolate v.
  void reduceDegree3(unsigned u, VertexCover& vc, list<unsigned>& chs);
  
  // Apply the Buss reduction rule to v: if a node has degree > k, where k is
  // the maximal size of the solution, then it is added to the solution vc,
  // and removed from the graph. It's neighourhood is added to chs.
  void reduceBuss(unsigned v, unsigned k, VertexCover& vc,
                  list<unsigned>& chs);

  // Returns true iff u != v, and u and v are neighbours, and every neighbor
  // of vertex u is either v, or also a neighbour of v, i.e. N(u) \cup {u}
  // \subsetq N(v) \cup {v}.
  bool checkTwins(unsigned u, unsigned v) const;

  // For each node v in vs, if v has a neighour u with checkTwins(u, v) ==
  // true, then add v to vc. If checkTwins(v, u) returns true, then add u to
  // vc. Add a list of changes nodes to chs.
  void reduceTwins(const list<unsigned>& vs, VertexCover& vc,
                   list<unsigned>& chs);

  // Add all nodes v to vc where there is another node u such
  // that checkTwins(u, v) returns true.
  void reduceTwins(VertexCover& vc);

  // Check if vertex v is unconfined, as defined by Akiba and Iwata in section
  // 5.3 of their paper, then add v to vc and N(v) to chs.
  void reduceUnconfined(unsigned v, VertexCover& vc, list<unsigned>& chs);

  // Check if vertex u and v form a duo (called a twin in the paper
  // Akiba+Iwata paper): d(u) = d(v) = 3 and N(u) = N(v). For now, we only
  // check.
  bool checkDuo(unsigned u, unsigned v) const;

  // Check if u and any v \in N(u) form a funnel, i.e. N(v) \ {u} induces a
  // clique. If so, then return {u} and {v} are alternatives, so reduce them,
  // and return true. Otherwise, return false. Add affected vertices to chs.
  bool reduceFunnel(unsigned u, Foldings& folds, list<unsigned>& chs);

  // Check if u is part of a desk: If (u, x, v, y) form a chordless 4-cycle
  // with all 4 vertices having at least degree 3, and N({u,v}) \cap N({x, y})
  // = \emptyset, and |N({u, v} \ {x, y}| <= 2 and |N({x, y} \ {u, v}| <= 2,
  // then A and B form a desk, which means they are alternatives. Reduce them,
  // and return true. Otherwise, return false.
  bool reduceDesk(unsigned u, Foldings& folds, list<unsigned>& chs);
  
  // Apply the crown reduction rule to the graph. A list of vertices that have
  // to be put into the solution will be added to vc.
  void reduceCrowns(VertexCover& vc);

  // Reduce based on an LP due to Nemhauser&Trotter. Construct a bipartite
  // Graph B such that ..., then find a maximum matching M on B using H-K, then
  // find a minimum VC from M using Koenig's Theorem, then solve the LP based
  // on that. Add vertices with solution 1 to vc, remove vertices with
  // solution 0 from G. Add affected vertices to chs.
  void reduceLP(VertexCover& vc, list<unsigned>& chs);
  
  // Apply the clique reduction rule to vertex v: If the graph induced by N(v)
  // forms a clique, then add N(v) to the solution vc, remove v from the
  // graph, and add N(N(v)) to chs. This is quite expensive O(|N(v)|^2) so it
  // should only be done for vertices with a modest degree, i.e. 3 or 4.
  void reduceClique(unsigned v, VertexCover& vc, list<unsigned>& chs);

  // Apply the clique reduction rule to all vertices with degree at least 3
  // and at most d. Add vertices that have to be in the solution to vc.
  void reduceClique(unsigned d, VertexCover& vc);
  
  // Apply the 1-degree and 2-degree rules exhaustively to the nodes in
  // vs. Vertices that have to go into the solution will be added to
  // vc. Folded vertices will be added to the front of folds. 
  void reduceLocal(const list<unsigned>& vs, VertexCover& vc,
                   Foldings& folds);

  // This applies various reduction rules which change the graph, but does NOT
  // shrink it by removing isolated vertices. At the moment, it guarantees
  // that
  // - the resulting graph has no vertices of degree 1.
  // - the resulting graph has no vertices of degree 2
  // ...
  // Adds to vc a list of vertices that have to be in the solution.
  // k is the maximal size of the solution. It is only used internally for
  // some reduction rules (Buss), reduce() may still return a more than k
  // elements in vc. Starts with checking only the nodes in vs, but ALL nodes
  // will be checked for the Busse rule.
  // This method is meant to be called for every branch, so it is modestly
  // expensive. 
  void reduce(const list<unsigned>& vs, VertexCover& vc, unsigned k,
              Foldings& folds);

  // Same as above but starts with checking ALL nodes.
  void reduce(VertexCover& vc, unsigned k, Foldings& folds);

  // Perform preprocessing on that graph. This will do everything that
  // reduce() does, but also perform a number of really expensive operations
  // that will only be done once before regular processing starts.
  // Adds the nodes that have to be in the solution to vc. Add all foldings to
  // folds, in the order they have to be done for unfolding (i.e. latest
  // folding first).
  void preprocess(VertexCover& vc, Foldings& folds);

  // Create a new instance containing all vertices from this instance except
  // for isolated vertices. Returns a pointer to the new instance, Caller is
  // reponsible for freeing the new instance. For each vertex v in the
  // returned Instance, invTransTable[v] contains the corresponding vertex
  // number in the original Instance.
  // Guarantees that vertex neighbour lists are still sorted in the shrunken
  // graph.
  Instance* shrink(vector<unsigned>& invTransTable) const;
  
  // Perform a simple greedy packing on this Instance, which is a 2-approx of
  // VertexCover (Alg 1 in the paper).
  //
  // A is an array of size m_nVertices + 1. Caller is responsible for
  // allocating and freeing the memory.
  //
  // Returns the size of the greedy packing in O(V*V + E) time. For each
  // vertex u, A[u] will be set to non-zero if u is in the solution, and to 0
  // if it is not.
  unsigned greedyPacking(unsigned A[]) const;

  // Perform a simple greedy packing on this Instance, which is a 2-approx of
  // VertexCover (Alg 1 in the paper).
  // Returns the size of the greedy packing in O(V*V + E) time.
  unsigned greedyPacking() const;

  // Max Vertex First approximation scheme with a worst-case factor of ln|V|
  // (Alg 2 in the paper).
  //
  // A is an array of size m_nVertices + 1. Caller is responsible for
  // allocating and freeing the memory.
  //
  // Returns the size of the approximate solution in O(V*V + E) time. For each
  // vertex u, A[u] will be set to non-zero if u is in the solution, and to 0
  // if it is not.
  unsigned approxMVF(unsigned A[]) const;

  // Max Vertex First approximation scheme with a worst-case factor of ln|V|
  // (Alg 2 in the paper).
  // Returns the size of the approximate solution in O(V*V + E) time.
  unsigned approxMVF() const;

  // Peforms yet another factor 2 approximation (Alg 3 in the paper).
  // 
  // Returns the size of the approximate solution in O(V + E) time.
  unsigned fracUB() const;

  // A lower bound approximation algorithm courtesy of Hua.
  //
  // Returns a lower bound on the solution size in O(V + E) time.
  unsigned hLB() const;

  // Find approx solution based on the hLB algorithm. This WILL change *this
  // so make sure to work on a copy. Assumes that *this has no isolated
  // vertices. 
  //
  // If an approx solution was found, then true is returned, and the solution
  // is appended to vc. Otherwise, false is returned, and vc will be left
  // unchanged.
  bool findHLBApprox(VertexCover& vc);

  // Clique cover lower bound (taken from Akiba+Iwata).
  //
  // Returns a lower bound on the solution size in O(V log V + E) time.
  unsigned cliqueCoverLB() const;
  
  // Partitions the graph into connected components. Returns the number of
  // components. For each vertex v, sets comp[v] to the number of this
  // vertex's component. Component numbering starts with 0. Complexity is O(V
  // + E).
  unsigned findConnectedComponents(unsigned* comp) const;

  // Gets a graph that only contains vertices and edges from component c of
  // *this as marked in comps, shrunk to remove all other vertices. Caller is
  // responsible for freeing the new graph. For each vertex v in the
  // return Instance, invTransTable[v] is set to the corresponding vertex
  // number in the original Instance.
  Instance* getComponent(const unsigned* comp, unsigned c,
                         vector<unsigned>& invTransTable) const;
  
  // Returns true if the graph is planar, and false if not.
  //bool planar() const;

  // Return a node degree histogram in hist: hist[d] will be set to the number
  // of vertices with degree d if the number if > 0. hist must have at least
  // size maxDegree() + 1.
  void getDegreeHistogram(unsigned* hist) const;
  
  // Write debug info about an instance, including its internal state.
  void dump(FILE* out) const;

  // Write some stats about the instance into a file.
  void dumpStats(FILE* out) const;
  
  // Read new instance from file. Returns NULL on error.
  static Instance* readInstance(FILE* f);

  // Write instance to stdout, using the input format.
  void write(FILE* out) const;
};

#endif // __INSTANCE_H
