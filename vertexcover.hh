#ifndef __VERTEXCOVER_H
#define __VERTEXCOVER_H

#include <boost/shared_ptr.hpp>
#include <list>

#include "util.hh"

// Forward declarations.
class Foldings;
class Instance;

// A class representing the set of vertices in a vertex cover. For now it's a
// list, it might well become a set later, or even a bool array of size n.
class VertexCover : private list<unsigned> {
public:
  typedef list<unsigned>::size_type size_type;
  typedef list<unsigned>::iterator iterator;
  typedef list<unsigned>::const_iterator const_iterator;
  
  VertexCover();
  VertexCover(const VertexCover& vc);
  ~VertexCover() {}

  // These are a few methods passed through from the underlying container.
  bool empty() const { return ((const list<unsigned>*) this)->empty(); }
  size_type size() const { return ((const list<unsigned>*) this)->size(); }
  const_iterator begin() const {
    return ((const list<unsigned>*) this)->begin();
  }
  iterator begin() {
    return ((list<unsigned>*) this)->begin();
  }
  const_iterator end() const {
    return ((const list<unsigned>*) this)->end();
  }
  iterator end() {
    return ((list<unsigned>*) this)->end();
  }
  iterator erase(iterator pos) { return ((list<unsigned>*) this)->erase(pos); }
  
  // Add a vertex to the cover. Does NOT make sure that the same vertex is not
  // added twice.
  void add(const unsigned v) { push_back(v); }

  // Add a list of vertices to the cover. Does NOT test for duplicates.
  void add(const list<unsigned>& l) { insert(end(), l.begin(), l.end()); }

  // Add another vertex cover to this one. Does NOT change for duplicates.
  void add(const VertexCover& o) { insert(end(), o.begin(), o.end()); }

  // Enforce uniqueness of all elements. Complexity depends on the underlying
  // container.
  void makeUnique();
  
  // Unfold all foldings in folds on *this. Caller is reponsible that folds is
  // in the correct order. It will be processed from front to back.
  void unfold(const Foldings& folds);

  // Write this cover to a FILE in a suitable.
  void write(FILE* out, const Instance& g) const;
};

#endif // __VERTEXCOVER_H
