#include <assert.h>
#include <stdio.h>

#include "util.hh"
#include "vertex.hh"

void Vertex::insertVertex(unsigned v) {
  Vertex::iterator i;

  // Make sure the ordering of the nodes is preserved.
  for (i = begin(); i != end(); i++) {
    if (*i >= v) break;
  }
  if (i == end()) {
    push_back(v);
  } else if (*i != v) {
    insert(i, v);
  }
}

void Vertex::removeVertex(unsigned v) {
  Vertex::iterator i;
  for (i = begin(); i != end(); i++)
    if (*i == v) {
      erase(i);
      return;
    }

  fprintf(stderr, "trying to remove non-existent node %u from edge list\n", v);
  throw(0);
}

bool Vertex::hasNeighbour(unsigned v) const {
  for (const_iterator i = begin(); i != end(); i++) {
    if (*i > v) return false;
    if (*i == v) return true;
  }

  return false;  
}

bool Vertex::hasDegree1() const {
  return (!empty() && ((++begin()) == end()));
}

bool Vertex::hasDegree2() const {
  return (!empty() && ((++begin()) != end()) && (++(++begin()) == end()));
}

unsigned Vertex::commonNeighbour(const Vertex& o) const {
  Vertex::const_iterator i = begin(), j = o.begin();

  while (i != end() && j != o.end()) {
    if (*i == *j) return *i;
    if (*i < *j)
      i++;
    else
      j++;
  }

  return 0;
}

unsigned Vertex::commonNeighbourExceptOne(const Vertex& o, unsigned v) const {
  Vertex::const_iterator i = begin(), j = o.begin();

  while (i != end() && j != o.end()) {
    if (*i == *j && *i != v) return *i;
    if (*i < *j)
      i++;
    else
      j++;
  }

  return 0;
}

void Vertex::commonNeighbours(const Vertex& o, list<unsigned>& cns) const {
  sorted_intersect(cns, *this, o);
}
