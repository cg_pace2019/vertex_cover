#ifndef __BRANCHING_HH
#define __BRANCHING_HH

#include <stdio.h>
#include <list>

class Instance;
class Vertex;

// Different branching methods. To keep this fast, we do NOT use a base class
// and inheritence. Rather, we define several static classes with the same
// interface, and switch between them with a macro in the main source
// file. This is not exactly pretty but much faster than calling virtual
// functions.

// Each branching class will need to define the following static methods:
//
// void initStats();
// void dumpStats(FILE* file);
// unsigned chooseBranchingSets(const Instance& g, list<unsigned>* bs);
//
// It should also define a macro of the form MAX_BRANCHES_<BRANCHING_NAME>.
//
// chooseBranchingSets(g, bs) shall choose the branching for graph g, shall
// store no more than MAX_BRANCHES_<BRANCHING_NAME> branching sets in bs, and
// return the number of branching sets stored.

class Branching147 {
  #define MAX_BRANCHES_Branching147 2

  static unsigned branchingCase1;
  static unsigned branchingCase2;

public:
  static void initStats();
  static void dumpStats(FILE* file);
  
  static unsigned chooseBranchingSets(const Instance& g, list<unsigned>* bs);
};




class Branching133 {
  #define MAX_BRANCHES_Branching133 3

  static unsigned branchCase21;
  static unsigned branchCase22;
  static unsigned branchCase3;
  static unsigned branchCase51;
  static unsigned branchCase52;
  static unsigned branchCase53;

  // Subcase 2.1: Mirror branching. If there are mirrors u \in N^2(v) with
  // N(v) \ N(u) inducing a clique or empty set, then branch into the set of
  // mirrors and v, or into N(v). Return true if branched, false otherwise.
  static bool subcase21(const Instance&g, list<unsigned>* bs, unsigned v);
  
  // Case 2. A vertex with degree >= 5. Tries mirror branching, otherwise
  // branches into v and N(v).
  static void case2(const Instance& g, list<unsigned>* bs, unsigned v);
  
  // If subcase 5.1 applies to vertex x and its neighbours a, b, and c, then
  // store the branches in bs, and return true. Otherwise, return false.
  static bool subcase51(const Instance& g, list<unsigned>* bs,
                        const Vertex& x, unsigned a, unsigned b, unsigned c);

  // If subcase 5.3 applies to vertex x and its neighbours a, b, and c, then
  // store the branches in bs, and return true. Otherwise, return false.
  static bool subcase53(list<unsigned>* bs, const Vertex& X,
                        unsigned a, const Vertex& A,
                        const Vertex& B, const Vertex& C);

  // If main case 5 applies to vertex x with |N(x)| == 3, then store the
  // branches in bs, and return the number of branches. Otherwise, return 0,
  // and leave bs untouched.
  static unsigned case5(const Instance& g, list<unsigned>* bs, unsigned x);
public:
  static void initStats();
  static void dumpStats(FILE* file);

  static unsigned chooseBranchingSets(const Instance& g, list<unsigned>* bs);
};

#endif // __BRANCHING_HH
