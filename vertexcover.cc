#include "folding.hh"
#include "vertexcover.hh"
#include "instance.hh"

VertexCover::VertexCover() : list<unsigned>() {
}

VertexCover::VertexCover(const VertexCover& o) : list<unsigned>(o) {
}

void VertexCover::makeUnique() {
  sort();
  unique();
}

void VertexCover::unfold(const Foldings& folds) {
  for (auto f = folds.begin(); f != folds.end(); f++) {
    (*f)->unfold(*this);
  }
}

void VertexCover::write(FILE* out, const Instance& g) const {
  fprintf(out, "s vc %u %lu\n", g.getNVertices(), size());
  list<unsigned> l;
  l.insert(l.end(), begin(), end());
  l.sort();
  for (list<unsigned>::const_iterator i = l.begin(); i != l.end(); i++) {
    fprintf(out, "%u\n", *i);
  }
}
