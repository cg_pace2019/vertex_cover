#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <signal.h>
#include <string.h>

#include "util.hh"

#include "vertex.hh"
#include "folding.hh"
#include "instance.hh"
#include "vertexcover.hh"
#include "branching.hh"

// Instrumentation.
#ifndef NDEBUG
unsigned long totalRecursions;
unsigned long prunedLowerBound;
unsigned long lowerBoundCarryOver;
unsigned long useGreedyPacking;
unsigned long useMVF;
unsigned long compOverBranch;
unsigned long foundHlbApprox;
unsigned long useCCLB;
unsigned long useHLB;
#endif // NDEBUG

#define BRANCHING Branching133
#define MAX_BRANCHES MAX_BRANCHES_Branching133

// Main recursion function. Tries to find a vertex cover of size k or
// less. Assumes that there are no loops.
// g         input graph.
// l         lower bound for the size of the vertex cover; this is only
//           indicative. 
// k         upper bound for the size of the vertex cover
// vc        if successful, the vertex cover will be stored in vc.
// rcs       set of initial candidates for reductions.
//
// Returns true if a vertex cover of size <=k was found, false if not.
bool findMinimalVC(Instance& g, unsigned l, unsigned k, VertexCover& vc,
                   const list<unsigned>& rcs) {
#ifndef NDEBUG
  totalRecursions++;
#endif
  
  // Apply reduction rules first.
  assert(vc.empty());
  Foldings folds;
  g.reduce(rcs, vc, k, folds);

  list<unsigned> chs;
  g.reduceTwins(rcs, vc, chs);

  // for (auto u = rcs.begin(); u != rcs.end(); u++)
  //  g.reduceUnconfined(*u, vc, chs);
  for (auto u = rcs.begin(); u != rcs.end(); u++) {
    g.reduceFunnel(*u, folds, chs);
    g.reduceDesk(*u, folds, chs);
  }
  
  //chs.sort(); chs.unique();
  g.reduceLocal(chs, vc, folds);

  assert(g.findDegree2Vertex() == 0);
  const unsigned reductSize = vc.size() + folds.unfoldSize();
  //fprintf(stderr, "reductions added %u vertices to the solution and stored %u in %u foldings, k= %u\n", vc.size(), folds.unfoldSize(), folds.size(), k);

  // Already too big?
  if (reductSize > k)
    return false;
  // Adjust limit
  k -= reductSize;
  if (l > reductSize)
    l -= reductSize;
  else
    l = 0;

  // Shrink graph by removing all isolated vertices. Vertex indices in the
  // shrunken graph need to be inverse translated using the table returned in
  // invTransTable!
  vector<unsigned> invTransTable(0);
  Instance* shrunk = g.shrink(invTransTable);
  //for (unsigned i = 1; i <= shrunk->getNVertices(); i++)
  //  fprintf(stderr, "%u ", invTransTable[i]);
  //fprintf(stderr, "\n");
  assert(invTransTable.size() == shrunk->getNVertices() + 1);

  // If the shrunk instance has no vertices, then we found a cover, and are
  // done. We still need to unfold.
  if (shrunk->getNVertices() == 0) {
    delete shrunk;
    vc.unfold(folds);
    return true;
  }

  // Pruning. Get lower and upper bounds on the solution size.
  //unsigned* greedySolution = new unsigned[shrunk->getNVertices() + 1];
  //const unsigned greedyApprox = shrunk->greedyPacking(greedySolution);
  unsigned* mvfSolution = new unsigned[shrunk->getNVertices() + 1];
  const unsigned mvfApprox = shrunk->approxMVF(mvfSolution);
  const unsigned hlb = shrunk->hLB();
  const unsigned cclb = 0; // shrunk->cliqueCoverLB();
  //const unsigned lowerBound = max(greedyApprox / 2, hlb);
  const unsigned lowerBound = max(cclb, hlb);
  //const unsigned upperBound = min(mvfApprox, greedyApprox);
  const unsigned upperBound = mvfApprox;

#ifndef NDEBUG
  if (lowerBound == cclb)
    useCCLB++;
  else
    useHLB++;
#endif
  
  // Adjust l.
  if (l < lowerBound)
    l = lowerBound;
  else {
#ifndef NDEBUG
    lowerBoundCarryOver++;
#endif
  }
  
  // If the l is greater than k, then we will fail, so give up. No need to
  // unfold here as vc will be discarded.
  //fprintf(stderr, "l = %u, k = %u\n", l, k);
  if (l > k) {
    delete [] mvfSolution;
    //delete [] greedySolution;
    delete shrunk;
#ifndef NDEBUG
    prunedLowerBound++;
#endif
    return false;
  }
  // If the lower bound is equal to the upper bound, then the upper bound is
  // tight, meaning that the approximation algorithm returned an exact
  // solution.
  if (l == upperBound) {
    // if (l == greedyApprox) {
    //   for (unsigned u = 1; u <= shrunk->getNVertices(); u++)
    //     if (greedySolution[u] != 0)
    //       vc.add(invTransTable[u]);
    //   useGreedyPacking++;
    // } else
      if (l == mvfApprox) {
      for (unsigned u = 1; u <= shrunk->getNVertices(); u++)
        if (mvfSolution[u] != 0)
          vc.add(invTransTable[u]);
#ifndef NDEBUG
      useMVF++;
#endif
    }
    delete [] mvfSolution;
    //delete [] greedySolution;
    delete shrunk;
    vc.unfold(folds);
    return true;    
  }
  delete [] mvfSolution;
  //delete [] greedySolution;
  // If the upper bound if less than k, then make it the new k.
  if (upperBound < k)
    k = upperBound;

  // Connected component analysis. If there is more than one component,
  // process them separately instead of branching. We only do this for graphs
  // of a reasonable size, and not in every branching.
  if (shrunk->getNVertices() > 20 && k % 3 == 0) {
    unsigned* const compColour = new unsigned[shrunk->getNVertices() + 1];
    const unsigned nComps = shrunk->findConnectedComponents(compColour);
    if (nComps > 1) {
      //fprintf(stderr, "Processing %u components instead of branching\n",
      //nComps);
#ifndef NDEBUG
      compOverBranch++;
#endif
      unsigned solSize = 0; 
      for (unsigned c = 0; c < nComps; c++) {
        VertexCover compSolution;
        
        vector<unsigned> compInvTrans;
        Instance* comp = shrunk->getComponent(compColour, c, compInvTrans);
        // fprintf(stderr, "Processing component %u: %u vertices\n",
        //         c, comp->getNVertices());
        
        //const unsigned compGreedy = comp->greedyPacking();
        const unsigned compMVF = comp->approxMVF();
        const unsigned compHlb = comp->hLB();
        //unsigned compUB = min(compGreedy, compMVF);
        unsigned compUB = compMVF;
        //const unsigned compLB = max(compGreedy / 2, compHlb);
        const unsigned compLB = compHlb;

        if (solSize + compLB > k) {
          delete comp;
          delete [] compColour;
          delete shrunk;
          return false;
        }
      
        VertexCover hlbApprox;
        Instance tmp = *comp;
        const bool foundHLBApprox = tmp.findHLBApprox(hlbApprox);
        if (foundHLBApprox) {
          //fprintf(stderr, "Found hlb approx solution of size %lu\n",
          //        hlbApprox.size());
#ifndef NDEBUG
          foundHlbApprox++;
#endif
          compUB = min(compUB, (unsigned) hlbApprox.size());
        } else {
          //fprintf(stderr, "Found no hlb approx solution\n");
        }
        if (foundHLBApprox && hlbApprox.size() == compLB ) {
          //fprintf(stderr, "hlb approx solution is tight, no recursion needed\n");
          for (auto i = hlbApprox.begin(); i != hlbApprox.end(); i++)
            compSolution.add(compInvTrans[*i]);
        } else {
          VertexCover rs;
          const bool success = findMinimalVC(*comp, compLB, compUB,
                                             rs, list<unsigned>());
          if (!success) {
            fprintf(stderr, "In graph:\n");
            shrunk->write(stderr);          
            fprintf(stderr, "Could not find a solution for component %u\n", c);
            comp->write(stderr);
          }
          assert(success);
          //fprintf(stderr, "Component %u solution size: %lu\n", c, rs.size());
          // Append reverse translated vertex indices.
          for (auto i = rs.begin(); i != rs.end(); i++)
            compSolution.add(compInvTrans[*i]);
        }
        delete comp;
        
        // Check if partial solution is already too large.
        solSize += compSolution.size();
        if (solSize > k) {
          //fprintf(stderr, "Partial solution size %u exceeds upper bound %u\n",
          //        solSize, k);
          delete [] compColour;
          delete shrunk;
          return false;
        }
      
        // Inverse translate component solution to the parent vertex namespace.
        for (auto i = compSolution.begin(); i != compSolution.end(); i++)
          vc.add(invTransTable[*i]);      
      }
      // Return solution.
      delete [] compColour;
      delete shrunk;
      vc.unfold(folds);
      return true;
    }
    delete [] compColour;
  }
  
  // Branching. The general idea is to build n branch sets, b[0..n-1], and
  // recurse for each of them, i.e. try to find a solution for the graphs
  // g \ b[0], g \ b[1], ..., respectively, and take the solution that is
  // smallest (including the branching set).
  list<unsigned> b[MAX_BRANCHES];
  
  const unsigned branches = BRANCHING::chooseBranchingSets(*shrunk, b);
  //for (unsigned i = 0; i < branches; i++) {
  //  fprintf(stderr, "Branch set %u: ", i); print_list(stderr, b[i]);
  //}
  
  // Iterate over all branches, except for the last one. If it finds a cover
  // that, including the branching set, is smaller than k, then keep it, and
  // set k to the size of this cover.
  unsigned bestBranch = UINT_MAX;
  VertexCover vcb[MAX_BRANCHES];
  for (unsigned i = 0; i < branches - 1; i++) {
    // If one branch has been successful already, then we only need to look
    // for better solutions than the one we found. In this case, lowerBound
    // has to be less than k because otherwise we cannot find a better
    // solution.
    if (b[i].size() <= k &&
        ((bestBranch == UINT_MAX) ? (l <= k) : (l < k))) {
      Instance gb = *shrunk;   // Copy shrunk.
      //fprintf(stderr, "branching with set ");
      //print_list(stderr, b[i]);
      list<unsigned> chs;
      gb.isolateVertices(b[i], chs);
      //chs.sort(); chs.unique();
      if (findMinimalVC(gb, l - b[i].size(), k - b[i].size(),
                        vcb[i], chs)) {
        // On success, keep the solution including the branching set
        vcb[i].add(b[i]);
        k = vcb[i].size();
        bestBranch = i;
      }
      //fprintf(stderr, "returned from branching\n");
    }
  }

  // Last branch. Branch only if it is not hopeless.
  if (b[branches - 1].size() <= k &&
      ((bestBranch == UINT_MAX) ? (l <= k) : (l < k))) {
    // For efficiency reasons, we use shrunk directly instead of copying it
    // again.
    //fprintf(stderr, "branching with set ");
    //print_list(stderr, b[branches - 1]);
    list<unsigned> chs;
    shrunk->isolateVertices(b[branches - 1], chs);
    //chs.sort(); chs.unique();
    if (findMinimalVC(*shrunk, l - b[branches - 1].size(),
                      k - b[branches - 1].size(), vcb[branches - 1], chs)) {
      vcb[branches - 1].add(b[branches - 1]);
      bestBranch = branches - 1;
    }
    //fprintf(stderr, "returned from branching\n");
  }
  delete shrunk;

  // If there was no successful branch, then return false.
  if (bestBranch == UINT_MAX)
    return false;
  
  // Append reverse translated vertex indices.
  for (VertexCover::const_iterator i = vcb[bestBranch].begin();
       i != vcb[bestBranch].end(); i++)
    vc.add(invTransTable[*i]);

  // Unfold.
  vc.unfold(folds);
  
  return true;
}

// Initialize stats
void initStats() {
#ifndef NDEBUG
  totalRecursions = 0;
  prunedLowerBound = 0;
  lowerBoundCarryOver = 0;
  useGreedyPacking = 0;
  useMVF = 0;
  compOverBranch = 0;
  foundHlbApprox = 0;
  useCCLB = 0;
  useHLB = 0;
  Instance::initStats();
  BRANCHING::initStats();
#endif // NDEBUG
}

// Print some internal statistics only useful for profiling.
void printStats(FILE* file, bool aborted) {
#ifndef NDEBUG
  fprintf(file, "%s %lu recursions\n", aborted ? "Aborted after" : "This took",
          totalRecursions);
  fprintf(file, "%lu recursions were stopped by the lower bound\n",
          prunedLowerBound);
  fprintf(file, "%lu recursions used the previous lower bound\n",
          lowerBoundCarryOver);
  fprintf(file, "%lu recursions ended up using greedy packing\n",
          useGreedyPacking);
  fprintf(file, "%lu recursions ended up using MVF\n", useMVF);
  fprintf(file, "%lu recursions used the CC lower bound\n", useCCLB);
  fprintf(file, "%lu recursions used the Hua lower bound\n", useHLB);
  fprintf(file, "%lu recursions ended up going after components instead of branching\n", compOverBranch);
  fprintf(file, "%lu components had an hlbApprox solution\n", foundHlbApprox);
  BRANCHING::dumpStats(file);
  Instance::printStats(file);
#endif // NDEBUG
}

// Catch SIGTERM: write some statistics to stderr and exit.
void handleSIGTERM(int) {
  fprintf(stderr, "Program terminated prematurely.\n");
#ifndef NDEBUG
  printStats(stderr, true);
#endif // NDEBUG
  
  exit(1);
}

int main(int, const char** const) {
  // Parse arguments and try to open input file.
  //if (argc < 2) {
  //  fprintf(stderr, "Usage: vertex_cover\n");
  //  return 1;
  //}
  
  //FILE *infile = stdin;
  // if ((infile = fopen(argv[1], "r")) == NULL) {
  //   fprintf(stderr, "Could not open input file %s\n", argv[1]);
  //   return 1;
  // }

  Instance* input = Instance::readInstance(stdin);
  if (!input) {
    fprintf(stderr, "Something went wrong reading the problem instance\n");
    return 1;
  }
  fprintf(stderr, "After dedup       : ");
  input->dumpStats(stderr);
  fprintf(stderr, "\n");
  
  // if (fclose(infile)) {
  //   fprintf(stderr, "Could not close input file\n");
  //   return 1;
  // }

  // Install signal handler to catch SIGTERM.
  struct sigaction action;
  memset(&action, 0, sizeof(struct sigaction));
  action.sa_handler = handleSIGTERM;
  sigaction(SIGTERM, &action, NULL);
  
  // Perform preprocessing.
#ifndef NDEBUG
  Instance::initStats();
#endif // NDEBUG
  VertexCover pre;
  Foldings folds;
  input->preprocess(pre, folds);
  for (VertexCover::const_iterator i = pre.begin(); i != pre.end(); i++)
    assert(input->getVertex(*i).hasDegree0());
  fprintf(stderr, "Found %lu vertices during preprocessing\n", pre.size());
  fprintf(stderr, "Found %lu folds with %u vertices\n",
          folds.size(), folds.unfoldSize());
  fprintf(stderr, "After preprocess  : ");
  input->dumpStats(stderr);
  fprintf(stderr, "\n");
#ifndef NDEBUG
  Instance::printStats(stderr);
#endif // NDEBUG
  
  // The final solution. Vertices from preprocessing are in for sure.
  VertexCover solution;
  solution.add(pre);
  
  // Shrink to remove isolated vertices.
  vector<unsigned> invTrans;
  Instance* const shrunk = input->shrink(invTrans);
  fprintf(stderr, "After shrinking   : ");
  shrunk->dumpStats(stderr);
  fprintf(stderr, "\n");
  
  // Only do more if there are any vertices left.
  if (shrunk->getNVertices() > 0) {
    // Find number of components.
    unsigned* const compColour = new unsigned[shrunk->getNVertices() + 1];
    const unsigned nComps = shrunk->findConnectedComponents(compColour);
    fprintf(stderr, "Connected components: %u\n", nComps);
    for (unsigned c = 0; c < nComps; c++) {
      VertexCover compSolution;
      
      vector<unsigned> compInvTrans;
      Instance* comp = shrunk->getComponent(compColour, c, compInvTrans);
      fprintf(stderr, "Processing component %u: %u vertices\n",
              c, comp->getNVertices());
      //comp->write(stdout);

      // Get approx solution.
      const unsigned greedy = comp->greedyPacking();
      fprintf(stderr, "Greedy packing solution size: %u\n", greedy);
      const unsigned mfv = comp->approxMVF();
      fprintf(stderr, "MVF solution size: %u\n", mfv);
      const unsigned hlb = comp->hLB();
      fprintf(stderr, "hLB lower bound: %u\n", hlb);
      const unsigned cliqueLB = comp->cliqueCoverLB();
      fprintf(stderr, "Clique cover lower bound: %u\n", cliqueLB);
      const unsigned lowerBound = max(cliqueLB, max(greedy / 2, hlb));
      fprintf(stderr, "Lower bound used: %u\n", lowerBound);
      unsigned upperBound = min(greedy, mfv);
      
      VertexCover hlbApprox;
      Instance tmp = *comp;
      const bool foundHLBApprox = tmp.findHLBApprox(hlbApprox);
      if (foundHLBApprox) {
        fprintf(stderr, "Found hlb approx solution of size %lu\n",
                hlbApprox.size());
        upperBound = min(upperBound, (unsigned) hlbApprox.size());
      } else {
        fprintf(stderr, "Found no hlb approx solution\n");
      }
      fprintf(stderr, "Upper bound used: %u\n", upperBound);
      
      if (foundHLBApprox && hlbApprox.size() == lowerBound) {
        fprintf(stderr, "hlb approx solution is tight, no recursion needed\n");
        for (auto i = hlbApprox.begin(); i != hlbApprox.end(); i++)
          compSolution.add(compInvTrans[*i]);
      } else {
        // Enter recursion.
        fprintf(stderr, "Entering recursion\n");
        VertexCover rs;
#ifndef NDEBUG
        initStats();
#endif
        const bool success = findMinimalVC(*comp, lowerBound, upperBound,
                                           rs, list<unsigned>());
        if (!success) {
          fprintf(stderr, "Could not find a solution for component %u\n", c);
          comp->write(stderr);
        }
        assert(success);
        fprintf(stderr, "Component %u solution size: %lu\n", c, rs.size());
#ifndef NDEBUG
        printStats(stderr, false);
#endif
        // Append reverse translated vertex indices.
        for (auto i = rs.begin(); i != rs.end(); i++)
          compSolution.add(compInvTrans[*i]);    
      }
      
      delete comp;

      // Inverse translate component solution to the parent vertex namespace.
      for (auto i = compSolution.begin(); i != compSolution.end(); i++)
        solution.add(invTrans[*i]);
    }
    delete [] compColour;
  }
  delete shrunk;

  fprintf(stderr, "Solution size before unfolding %lu, including %lu from preprocessing and %lu from component recursion\n",
          solution.size(), pre.size(), solution.size() - pre.size());

  // Unfold.
  solution.unfold(folds);
  fprintf(stderr, "Found a solution of size %lu after unfolding\n",
          solution.size());

  // Write output.
  //fprintf(stdout, "c This is the solution for input file %s\n", argv[1]);
  solution.write(stdout, *input);
  
  delete input;
  
  return 0;
}
