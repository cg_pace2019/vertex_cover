#include "folding.hh"
#include "vertexcover.hh"

unsigned Foldings::unfoldSize() const {
  unsigned s = 0;
  
  for (auto f = begin(); f != end(); f++)
    s += (*f)->unfoldSize();

  return s;
}

Deg2Folding::Deg2Folding(unsigned u_, unsigned v_, unsigned w_) :
  Folding(), u(u_), v(v_), w(w_) {
}

void Deg2Folding::unfold(VertexCover& vc) const {
  // Is folded vertex in alread in *this?
  auto i = vc.begin();
  while (i != vc.end()) {
    if (*i == u) break;
    i++;
  }
  // If u is in vc, then replace it with v and w, otherwise add u to vc.
  if (i != vc.end()) {
    vc.erase(i);
    vc.add(v);
    vc.add(w);
  } else {
    vc.add(u);
  }
}




FunnelFolding::FunnelFolding(unsigned u_, unsigned v_,
                             const list<unsigned>& Suv_,
                             const list<unsigned>& NBwoNA_) :
  Folding(), u(u_), v(v_), Suv(Suv_), NBwoNA(NBwoNA_) {
}

void FunnelFolding::unfold(VertexCover& vc) const {
  // fprintf(stderr, "Unfolding u = %u, v = %u, intersect = [ ", u, v);
  // print_list(stderr, Suv, false);
  // fprintf(stderr, " ], N(v) \\ N[u] = [ ");
  // print_list(stderr, NBwoNA, false);
  // fprintf(stderr, " ]\n");
  // fprintf(stderr, "Vertex cover before unfolding: ");
  // print_list(stderr, (list<unsigned>&) vc);
  
  // Check if NBwoNA is in vc. TODO: NBwoNA is sorted. If vc was also sorted,
  // this could be done in O(|NBwoNA| + |vc|) instead of O(|NBwoNA| * |vc|).
  auto x = NBwoNA.begin();
  for (; x != NBwoNA.end(); x++) {
    auto y = vc.begin();
    for (; y != vc.end(); y++)
      if (*x == *y) break;
    if (y == vc.end()) break;
  }
  if (x == NBwoNA.end()) {
    //fprintf(stderr, "Case 1 happened\n");
    // NBwoNA is in vc, so we add Suv and u to vc.
    vc.add(Suv);
    vc.add(u);
  } else {
    //fprintf(stderr, "Case 2 happened\n");
    // NBwoNA is not in vc, so we add Suv and v to vc.
    vc.add(Suv);
    vc.add(v);
  }
  // fprintf(stderr, "Vertex cover after unfolding: ");
  // print_list(stderr, (list<unsigned>&) vc);
}




DeskFolding::DeskFolding(unsigned a1_, unsigned a2_, unsigned b1_, unsigned b2_,
                         const list<unsigned>& NBwoNA_) :
  a1(a1_), a2(a2_), b1(b1_), b2(b2_), NBwoNA(NBwoNA_) {
}

void DeskFolding::unfold(VertexCover& vc) const {
  // Check if NBwoNA is in vc. TODO: NBwoNA is sorted. If vc was also sorted,
  // this could be done in O(|NBwoNA| + |vc|) instead of O(|NBwoNA| * |vc|).
  auto x = NBwoNA.begin();
  for (; x != NBwoNA.end(); x++) {
    auto y = vc.begin();
    for (; y != vc.end(); y++)
      if (*x == *y) break;
    if (y == vc.end()) break;
  }  
  if (x == NBwoNA.end()) {
    //fprintf(stderr, "Case 1 happened\n");
    // NBwoNA is in vc, so we add Suv and u to vc.
    vc.add(a1);
    vc.add(a2);
  } else {
    //fprintf(stderr, "Case 2 happened\n");
    // NBwoNA is not in vc, so we add Suv and v to vc.
    vc.add(b1);
    vc.add(b2);
  }
}
