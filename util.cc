#include "util.hh"

void print_list(FILE* f, const list<unsigned>& l, bool newl) {
  for (list<unsigned>::const_iterator i = l.begin(); i != l.end(); i++)
    fprintf(f, "%u ", *i);

  if (newl)
    fprintf(f, "\n");
}

void sorted_insert_unique(list<unsigned>& l, unsigned v) {
  list<unsigned>::iterator i;

  // Make sure the ordering of the nodes is preserved.
  for (i = l.begin(); i != l.end(); i++) {
    if (*i >= v) break;
  }
  if (i == l.end()) {
    l.push_back(v);
  } else if (*i != v) {
    l.insert(i, v);
  }
}


void sorted_merge_unique(list<unsigned>& a, const list<unsigned>& b) {
  // auto i = a.begin();
  // auto j = b.begin();
  
  // // Step 1: prepend elements in b that are smaller than the first element in
  // // a to a.
  // if (i != a.end())
  //   while (*j < *i) j++;
  // a.insert(a.begin(), b.begin(), j);

  // // Now, j == b.end() or i == a.end() or *j >= *i.
  // assert(j == b.end() || i == a.end() || *j >= *i);
  
  // // Step 2: interleave elements from a and b until all remaining elements in
  // // b are greater than the last element in b, removing duplicates.
  // while (i != a.end() && j != b.end()) {
  //   // Avoid duplicates.
  //   if (*i == *j) { i++; j++; continue; }

  //   // If j is ahead of i, catch up.
  //   if (*i < *j) { i++; continue; }

  //   // i is ahead of j, so catch up.
  //   assert(*j < *i);
    
  // }
  
  // // Step 3: append all remaining elements in b to a.
  
  // TODO: Make faster by not using merge(). In particular, the call to
  // unique() and the use of tmp both can be avoided by inserting new elements
  // directly into a, avoiding duplicates in the process.
  list<unsigned> tmp = b;
  a.merge(tmp);
  a.unique();
}

void sorted_diff(list<unsigned>& a, const list<unsigned>& b) {
  auto i = a.begin();
  auto j = b.begin();

  while (i != a.end() && j != b.end()) {
    if (*i == *j)
      i = a.erase(i);
    else if (*i < *j)
      i++;
    else
      j++;
  }    
}

void sorted_intersect(list<unsigned>& s,
                      const list<unsigned>& a, const list<unsigned>& b) {
  auto i = a.begin();
  auto j = b.begin();

  while (i != a.end() && j != b.end()) {
    if (*i == *j) s.push_back(*i);
    if (*i < *j)
      i++;
    else
      j++;
  }  
}
