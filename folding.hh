#ifndef __FOLDING_H
#define __FOLDING_H

#include <boost/shared_ptr.hpp>
#include <list>

using namespace std;

// Forward declarations.
class VertexCover;

// This is an abstract base class that represents a postprocessing operation
// that needs to be performed on a vertex cover because of a reduction
// rule. It is used, among others, by the degree-2-disjoint rule, the
// alternatives rules (funnel and desk), and the duo rule.
class Folding {
public:
  virtual ~Folding() {}

  // Reverse this folding on vc.
  virtual void unfold(VertexCover& vc) const = 0;

  // Return the number of vertices that will be added to a VertexCover when
  // unfolding this Folding.
  virtual unsigned unfoldSize() const = 0;
};

// A LIFO container for Foldings.
class Foldings : private list<shared_ptr<Folding> > {
  typedef list<shared_ptr<Folding> > base_type;
public:
  typedef list<shared_ptr<Folding> >::size_type size_type;
  typedef list<shared_ptr<Folding> >::iterator iterator;
  typedef list<shared_ptr<Folding> >::const_iterator const_iterator;

  // Methods inherited from the underlying container. Iteration will be in
  // LIFO order, i.e. start with the last element added using add().
  bool empty() const { return ((const base_type*) this)->empty(); }
  size_type size() const { return ((const base_type*) this)->size(); }
  const_iterator begin() const { return ((const base_type*) this)->begin(); }
  iterator begin() { return ((base_type*) this)->begin(); }
  const_iterator end() const { return ((const base_type*) this)->end(); }
  iterator end() { return ((base_type*) this)->end(); }
  
  // Add f to the front of the unfolding order. f now belongs to us.
  void add(Folding* f) { push_front(value_type(f)); }

  // Return the number of vertices by which a VertexCover will grow when
  // processing this.
  unsigned unfoldSize() const;
};

// Used by Instance::reduceDegree2() (the disjoint part).
class Deg2Folding : public Folding {
private:
  unsigned u;    // The original degree 2 vertex.
  unsigned v;    // First neighbour.
  unsigned w;    // Second neighbour.

public:
  Deg2Folding(unsigned u, unsigned v, unsigned w);

  virtual ~Deg2Folding() {}

  // If u \in vc, then replace it with v and w. Otherwise, add u to vc.
  virtual void unfold(VertexCover& vc) const;

  // Solution will always grow by 1.
  virtual unsigned unfoldSize() const { return 1; }
};

// Used by Instance::reduceFunnel().
class FunnelFolding : public Folding {
private:
  // The vertices. A = {u} and B = {v}.
  unsigned u;
  unsigned v;
  // Intersection of N(u) and N(v) aka N(A) \cap N(B).
  list<unsigned> Suv;
  // N(v) \ N[u] aka N(B) \ N[A].
  list<unsigned> NBwoNA;
public:
  FunnelFolding(unsigned u, unsigned v,
                const list<unsigned>& Suv, const list<unsigned>& NBwoNA);

  virtual ~FunnelFolding() {}

  // If NBwoNA is in vc, then add u and Suv to vc. Otherwise, add v and Suv to
  // vc.
  virtual void unfold(VertexCover& vc) const;
  
  // Solution will grow by 1 + the size of the intersection.
  virtual unsigned unfoldSize() const { return 1 + Suv.size(); }
};

// Used by Instance::reduceDesk().
class DeskFolding : public Folding {
private:
  // The vertices. A = { a1, a2 } and B = { b1, b2 }.
  unsigned a1;
  unsigned a2;
  unsigned b1;
  unsigned b2;

  // Intersection of N(A) an N(B) is always empty for desks.
  // N(B) \ N[A] = N(B) \ A for desks.
  list<unsigned> NBwoNA;
public:
  DeskFolding(unsigned a1, unsigned a2, unsigned b1, unsigned b2,
              const list<unsigned>& NBwoNA);

  virtual ~DeskFolding() {}

  // If NBwoNA is in vc, then add a1 and a2 to vc. Otherwise, add b1 and b2 to
  // vc.
  virtual void unfold(VertexCover& vc) const;

  // Solution will grow by 2.
  virtual unsigned unfoldSize() const { return 2; }
};
  
#endif // __FOLDING_H
