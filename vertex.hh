#ifndef __VERTEX_H
#define __VERTEX_H

#include <list>

using namespace std;

// A vertex in a graph. At the moment, we use STL lists as the underlying data
// structure, but this may change to something else (like a set) in the
// future. An edge list is sorted. Index v begin in an edge list means that
// this vertex has an edge to vertex v. TODO: Switch to forward_list<>? set<>?
class Vertex : public list<unsigned> {
public:
  typedef list<unsigned>::iterator iterator;
  typedef list<unsigned>::const_iterator const_iterator;

  Vertex() : list<unsigned>() { }
  Vertex(const Vertex& o) : list<unsigned>(o) { }
  ~Vertex() {}

  // Insert a vertex assuming that it is the vertex with the highest number,
  // and unique. This is much faster than insertVertex().
  void insertVertexUnsafe(unsigned v) { push_back(v); }
  
  // Insert a vertex, preserving order. If it already exists, do nothing.
  void insertVertex(unsigned v);

  // Remove a vertex. If the vertex does not exists, fail.
  void removeVertex(unsigned v);

  // Returns true iff vertex v is a neighbour of this vertex.
  bool hasNeighbour(unsigned v) const;

  // Returns true iff this vertex is isolated.
  bool hasDegree0() const { return empty(); }
  
  // Returns true iff this vertex has exactly one neighbour.
  bool hasDegree1() const;

  // Returns true iff this vertex has exactly two neighbours.
  bool hasDegree2() const;

  // If *this and o share at least one neighbour, then return a common
  // neighbour. Otherwise, return 0.
  unsigned commonNeighbour(const Vertex& o) const;

  // Same as above, but the common neigbour must not be v.
  unsigned commonNeighbourExceptOne(const Vertex& o, unsigned v) const;

  // Add all common neighbours of *this and o to cns.
  void commonNeighbours(const Vertex& o, list<unsigned>& cns) const;
};

#endif // __VERTEX_H
