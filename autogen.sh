#!/bin/sh
# Copyright (C) 2019  Jiehua Chen & Sven Grottke
# 
# You should have received a copy of the GNU General Public License
# along with this code; see the file COPYING.  If not, see
# <http://www.gnu.org/licenses/>.
# 

# very simple autogen.sh that runs aclocal, autoconf etc. after
# checkout to create the complete package
# usage: 'autogen.sh' to create all necessary files
#        'autogen.sh clean' to remove all generated files

# generic error function
die() {
    echo $0
    exit 1
}

# cleaner function; will call make maintainer-clean if Makefile
# exists
clean() {
    [ -r Makefile ] && {
        # let make do all the dirty work
        echo 'make maintainer-clean'
        make maintainer-clean || {
            die "make maintainer-clean failed"
        }
    } 
    # try to do it ourselves
    echo 'removing files'
    rm -Rf Makefile config.h stamp-h1 config.status config.cache\
        config.log autom4te.cache || {
        die "rm failed"
    }
    
    # we only have to clean up the leftovers not generated bu configure
    rm -f aclocal.m4 config.h.in configure depcomp install-sh missing\
        mkinstalldirs Makefile.in || {
        die "rm failed"
    }

    return 0
}

# generate all necessary stuff
generate() {
    aclocal || die "aclocal failed"
    autoheader || die "autoheader failed"
    automake --foreign --add-missing || die "automake failed"
    autoconf || die "autoconf failed"

    return 0
}

# check args
[ "$#" -eq "1" ] && [ "$1" = "clean" ] && clean && exit 0

# generate
generate
