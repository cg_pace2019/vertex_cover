#ifndef __UTIL_H
#define __UTIL_H

#include <stdio.h>

#include <list>

using namespace std;

// Various utility functions.


// Dump l to f, separated by spaces. If newl is true, add a newline.
void print_list(FILE* f, const list<unsigned>& l, bool newl = true);

// Insert an v into sorted list l, preserving the sorting order. No
// duplicates of existing elements will be inserted.
void sorted_insert_unique(list<unsigned>& l, unsigned x);

// Merge sorted list b into sorted list a. The resulting list a will be
// sorted w/o duplicate elements. This is the equivalent of calling
// { a.insert(a.end(), b.begin(), b.end()); a.sort(); a.unique(); }
// Running time: O(|a| + |b|)
void sorted_merge_unique(list<unsigned>& a, const list<unsigned>& b);

// Remove all elements in sorted list b from sorted list a.
void sorted_diff(list<unsigned>& a, const list<unsigned>& b);

// Add all elements that are both in sorted list a and in sorted list b to s.
void sorted_intersect(list<unsigned>& s,
                      const list<unsigned>& a, const list<unsigned>& b);

#endif // __UTIL_H
