#include <stdio.h>
#include <assert.h>
#include <limits.h>
#include <stdlib.h>

#include "util.hh"
#include "vertex.hh"
#include "instance.hh"
#include "branching.hh"

unsigned Branching147::branchingCase1;
unsigned Branching147::branchingCase2;

void Branching147::initStats() {
  branchingCase1 = 0;
  branchingCase2 = 0;
}

void Branching147::dumpStats(FILE* file) {
  fprintf(file, "Branching case 1: %u\n", branchingCase1);
  fprintf(file, "Branching case 2: %u\n", branchingCase2);
}

unsigned Branching147::chooseBranchingSets(const Instance& g,
                                           list<unsigned>* bs) {
  // A branching rule with O(1.47^k): if there is a degree 2 vertex, then
  // either put it and all neighbours of its two neighbours into the solution,
  // or put its two neighbours into the solution; otherwise, find the vertex
  // with the highest degree, and put itself in one branch, and all its
  // neighbours in the other.
  unsigned bv = g.findDegree2Vertex();
  if (bv != 0) {
    const Vertex& v = g.getVertex(bv);
    assert(v.size() == 2);
    bs[0].push_back(bv);
    const unsigned n1 = v.front();
    const unsigned n2 = v.back();
    bs[0].insert(bs[0].end(), g.getVertex(n1).begin(), g.getVertex(n1).end());
    bs[0].insert(bs[0].end(), g.getVertex(n2).begin(), g.getVertex(n2).end());
    bs[0].sort(); bs[0].unique();
    bs[1].push_back(n1);
    bs[1].push_back(n2);
  } else {
    bv = g.maxDegreeVertex();
    bs[0].push_back(bv);
    bs[1].insert(bs[1].end(), g.getVertex(bv).begin(), g.getVertex(bv).end());
  }

  return 2;
}





unsigned Branching133::branchCase21;
unsigned Branching133::branchCase22;
unsigned Branching133::branchCase3;
unsigned Branching133::branchCase51;
unsigned Branching133::branchCase52;
unsigned Branching133::branchCase53;

void Branching133::initStats() {
  branchCase21 = 0;
  branchCase22 = 0;
  branchCase3 = 0;
  branchCase51 = 0;
  branchCase52 = 0;
  branchCase53 = 0;
}

void Branching133::dumpStats(FILE* file) {
  fprintf(file, "Branching case 2.1 (mirror): %u times\n", branchCase21);
  fprintf(file, "Branching case 2.2: %u times\n", branchCase22);
  fprintf(file, "Branching case 3: %u times\n", branchCase3);
  fprintf(file, "Branching case 5.1: %u times\n", branchCase51);
  fprintf(file, "Branching case 5.2: %u times\n", branchCase52);    
  fprintf(file, "Branching case 5.3: %u times\n", branchCase53);    
}

bool Branching133::subcase21(const Instance& g, list<unsigned>* bs,
                             unsigned v) {
  // Find all vertices with distance 2 from v, i.e. N(N(v)) \ (N(v) \cup {v}).
  const Vertex& V = g.getVertex(v);
  list<unsigned> N2;
  g.getOpenNeighbourhood(V, N2);
  sorted_diff(N2, V);
  N2.remove(v);

  // For each vertex u \in N2, check whether it is a mirror of v. Mirrors will
  // be added to the first branch.
  bool foundMirrors = false;
  for (auto u = N2.begin(); u != N2.end(); u++) {
    list<unsigned> diff = V;
    sorted_diff(diff, g.getVertex(*u));
    bool isClique = true;
    // Check whether diff induces a clique (or empty set).
    for (auto x = diff.begin(); x != diff.end(); x++) {
      // Speedup neigbourhood tests. We do not need to search the
      // neighbourhood of x from the beginning for each hasEdge() test,
      // because we know that 1) *y > *x and 2) we know that the previous *y
      // has an edge with *x.
      const Vertex& X = g.getVertex(*x);
      auto z = X.begin();
      for (auto y = x; y != diff.end(); y++) {
        if (*x == *y) continue;
        while (z != X.end() && *z < *y)
          z++;
        if (z == X.end() || *z != *y) {
          isClique = false;
          break;
        }
      }
      if (!isClique) break;
    }
    if (isClique) {
      foundMirrors = true;
      bs[0].push_back(*u);
    }
  }
  if (foundMirrors) {
    bs[0].push_back(v);
    bs[1].insert(bs[1].end(), V.begin(), V.end());
    return true;
  }
  
  return false;
}

void Branching133::case2(const Instance& g, list<unsigned>* bs, unsigned v) {
  // Check mirrors.
  // if (subcase21(g, bs, v)) {
  //   branchCase21++;
  //   return;
  // }

  // Branch into v and N(v).
  const Vertex& V = g.getVertex(v);
  bs[0].push_back(v);
  bs[1].insert(bs[1].end(), V.begin(), V.end());
  branchCase22++;
  return;
}

bool Branching133::subcase51(const Instance& g, list<unsigned>* bs,
                             const Vertex& x,
                             unsigned a, unsigned b, unsigned c) {
  // If there is more than one triangle, we pick the one where the third
  // neighbour has the highest degree.
  unsigned third = 0;
  unsigned thirdDeg = 0;
  unsigned d;
  if (g.hasEdge(a, b)) {
    third = c;
    thirdDeg = g.getVertex(c).size();
  }
  if ((d = g.getVertex(b).size() > thirdDeg) && g.hasEdge(a, c)) {
    third = b;
    thirdDeg = d;
  }
  if ((d = g.getVertex(a).size() > thirdDeg) && g.hasEdge(b, c)) {
    third = a;
    thirdDeg = d;
  }

  // Did we find a triangle?
  if (third != 0) {
    bs[0].insert(bs[0].end(), x.begin(), x.end());
    bs[1].insert(bs[1].end(), g.getVertex(third).begin(),
                 g.getVertex(third).end());
    return true;
  }
  
  return false;
}

bool Branching133::subcase53(list<unsigned>* bs, const Vertex& X,
                             unsigned a, const Vertex& A,
                             const Vertex& B, const Vertex& C) {
  if (A.size() != 4) return false;
  
  bs[0].insert(bs[0].end(), X.begin(), X.end());
  bs[1].insert(bs[1].end(), A.begin(), A.end());

  bs[2].push_back(a);
  bs[2].insert(bs[2].end(), B.begin(), B.end());
  bs[2].insert(bs[2].end(), C.begin(), C.end());
  bs[2].sort(); bs[2].unique();
  
  return true;
}

unsigned Branching133::case5(const Instance& g, list<unsigned>* bs,
                             unsigned x) {
  const Vertex& X = g.getVertex(x);
  assert(X.size() == 3);
  
  // We need x's neighbours.
  Vertex::const_iterator i = X.begin();
  const unsigned a = *(i++);
  const unsigned b = *(i++);
  const unsigned c = *i;
  
  // Subcase 5.1: If there is an edge between any two of x's neighbours,
  // then branch into N(x) and N(d) where d is the third neighbour.
  if (subcase51(g, bs, X, a, b, c)) {
    branchCase51++;
    return 2;
  }

  // Subcase 5.2: If there is a cycle connecting x, two of its neighbours,
  // and d (which cannot be a neighbour of x because of subcase 5.1), then
  // branch according to N(x) and {x,d}.
  unsigned d;
  if ((d = g.getVertex(a).commonNeighbourExceptOne(g.getVertex(b), x)) != 0) {
    assert(x != d);
    bs[0].insert(bs[0].end(), X.begin(), X.end());
    bs[1].push_back(x);
    bs[1].push_back(d);
    branchCase52++;
    return 2;
  }
  if ((d = g.getVertex(a).commonNeighbourExceptOne(g.getVertex(c), x)) != 0) {
    assert(x != d);
    bs[0].insert(bs[0].end(), X.begin(), X.end());
    bs[1].push_back(x);
    bs[1].push_back(d);
    branchCase52++;
    return 2;
  }
  if ((d = g.getVertex(b).commonNeighbourExceptOne(g.getVertex(c), x)) != 0) {
    assert(x != d);
    bs[0].insert(bs[0].end(), X.begin(), X.end());
    bs[1].push_back(x);
    bs[1].push_back(d);
    branchCase52++;
    return 2;
  }

  // TODO: always try to find a 5.1 before a 5.2 before a 5.3.
  
  // Subcase 5.3: Subcases 5.1 and 5.2 do not apply, and |N(a)| (or b or c)
  // == 4. Then we branch according to N(x) and N(a) (or b or c) and {a}
  // \cup N(b) \cup N(c). TODO: probiere zuerst den mit der groessten N().
  const Vertex& A = g.getVertex(a);
  const Vertex& B = g.getVertex(b);
  const Vertex& C = g.getVertex(c);
  if (subcase53(bs, X, a, A, B, C)) {
    branchCase53++;
    return 3;
  }
  if (subcase53(bs, X, b, B, A, C)) {
    branchCase53++;
    return 3;
  }
  if (subcase53(bs, X, c, C, B, A)) {
    branchCase53++;
    return 3;
  }

  // No subcase applied to this vertex.
  return 0;
}

unsigned Branching133::chooseBranchingSets(const Instance& g,
                                           list<unsigned>* bs) {
  // A branching rule taken from p.99 of Rolf's book. It has worst-case
  // complexity of O(1.33**k). We use a slightly simplified version as our
  // reduction rules guarantee the absence of vertices with degree 2 or less.

  // Get vertices with lowest and highest degree.
  unsigned minV, maxV;
  g.getNonIsoMinMaxDegreeVertices(minV, maxV);
  if (minV == 0 && maxV == 0) return 0;
  
  const Vertex& minv = g.getVertex(minV);
  const Vertex& maxv = g.getVertex(maxV);

  // Exclude a few impossible cases, including main cases 1 and 4.
  assert(minv.size() > 2);

  // Main case 2: If deg(maxv) >= 5, then branch into maxv and N(maxv).
  if (maxv.size() >= 5) {
    case2(g, bs, maxV);
    return 2;
  }

  // Main case 3: Regular graph. Branch into maxv and N(maxv).
  if (minv.size() == maxv.size()) {
    bs[0].push_back(maxV);
    bs[1].insert(bs[1].end(), maxv.begin(), maxv.end());
    branchCase3++;
    return 2;
  }
  
  // Main case 4: there is a vertex with degree 2. This cannot happen because
  // of the preprocessing. 

  // Main case 5: there is a vertex with degree 3. Such a vertex must exist at
  // this time because of the previous cases not applying.
  unsigned nb5;
  
  // Try minV first.
  nb5 = case5(g, bs, minV);
  if (nb5 != 0)
    return nb5;

  // No luck. We have to try all degree 3 vertices.
  for (unsigned x = 1; x <= g.getNVertices(); x++) {
    if (g.getVertex(x).size() != 3) continue;
    nb5 = case5(g, bs, x);
    if (nb5 != 0) return nb5;
  }
  fprintf(stderr, "No branching case applied. Ooops.\n");
  assert(false);
  return 0;
}

